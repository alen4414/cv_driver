Script VersionCheck
return "common.qs 1.0.1";
EndScript

Script JM_CommonReply
(o2, ip, port, txid)

	U4 ret, ticket;
	
	o2["TxID"] = txid;
	//o2["TimeStamp"] = CurrentFT();
	o2["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
	Send@JM(:JM, ToJSON(o2), ip, port, ticket, ret);

EndScript

Script CTM_Unit
CV cv;
U4 Status = 1;
EndScript

Script CThreadManager
CV cv;
Map map;		//Key is the task name, and value is the CTM_Unit

	Script Run
		(name)

		U4 ret;

		CVLock(cv);
		if (MapCheck(map, name))
		{
			CVLock(map[name].cv);
			if (map[name].Status)
			{
				CVUnlock(map[name].cv);
				ret = -1;
			}
			else
			{
				map[name].Status = 1;
				CVUnlock(map[name].cv);
				ret = 0;
			}
		}
		else
		{
			PrintLn(name, " first time");
			MapAddEx(map, name, "CTM_Unit");
			ret = 0;
		}
		CVUnlock(cv);
	return ret;
	
	EndScript
	
	Script Stop
		(name)
		CVLock(cv);
		if (MapCheck(map, name))
		{
			CVLock(map[name].cv);
			map[name].Status = 0;
			CVUnlock(map[name].cv);
		}
		else
		{
			PrintLn("Bad name is specified for ThreadManager, ", name);
		}
		CVUnlock(cv);
	EndScript

EndScript

Script RunFile
(fname)
String s;
U4 ret;
StrReadFile(s, fname, ret);
if (ret == 0)
{
	RunAdhocScript(s);
}
EndScript

Script Init_Lib
(lib, alias)

String windows, linux;
U4 ret;
String v;

LibLoad(LinWin(StrAssign(linux, "./lib", lib, ".so"), StrAssign(windows, lib, ".dll")), alias, ret);
if (ret && ret != 4)		//for now at development stage, ignore the case where external lib is already loaded
{
	PrintLn (ScriptName(), " " ,LinWin(linux, windows), " LibLoad for failed with ", ret);
	throw;		//lib failed to load
}

PrintLn(alias, " init returns ", ret);

EndScript

Script CommonInit

String s;
U4 ret;
U4 loop, n;

String FileName, FirstLine, TimeStampFormat, CloseName;
U8 MaxFileSize;
U4 TimeOption, LogLevel;
String Handler, License;

String IP, LocalIP;
U4 LocalPort, Port, Enable;

String tmpString, key3;
Reference p0, p1, p2, p3;
U0 dummy_number;
String section_name, index_name, item_name;

StrReadFile(s, "Config.json", ret);
if (ret)
{
	Print("Failed to read 'Config.json'");
	return;
}

GCreateEx("Config", "Object");
JSONParse(:Config, s, ret);

//PrintLn(:Config["KERNEL"]["RemoteIP"],"qwewqeqweqw");

if (ret)
{
	PrintLn("Failed to parse 'Config.json', ", ret);
	return;
}

Init_Lib("QLog", "QLog");

foreach (p0, :Config, dummy_number, section_name)
{
	switch (section_name)
	{
	case "Log"
		foreach(p1, p0, dummy_number, index_name)
		{
			//default setting
			StrAssign(FileName, "Log/", index_name, "_%Y%M%D.txt");
			FirstLine = "";
			TimeStampFormat = "%T ";
			CloseName = ".log";
			MaxFileSize = 0;
			TimeOption = 3;
			LogLevel = 0;
			
			foreach(p2, p1, dummy_number, item_name)
			{
				switch (item_name)
				{
				case "FileName"
					FileName = p2;
					break;
					
				case "FirstLine"
					FirstLine = p2;
					break;
					
				case "TimeStamp"
					TimeStampFormat = p2;
					break;
					
				case "CloseName"
					CloseName = p2;
					break;
					
				case "LogLevel"
					LogLevel = p2;
					break;
				
				case "TimeOption"
					TimeOption = p2;
					break;
					
				case "MaxFileSize"
					MaxFileSize = p2;
					break;
				}
			}
			
			GCreateEx2(index_name, "QLog", "QLog");
			Set@QLog(:index_name, FileName, TimeStampFormat, MaxFileSize, TimeOption, FirstLine, ret);
			SetCloseName@QLog(:index_name, CloseName);
			SetLogLevel@QLog(:index_name, LogLevel);
			Start@QLog(:index_name);
		}
		break;
		
	case "SECS"
		Init_Lib("JSECS", "Secs");
		foreach (p1, p0, dummy_number, index_name)
		{
			ConfigureSECS(index_name, p1);
		}
		break;
		
	case "UI"
		IP = "127.0.0.1";
		Port = 3000;
		foreach(p1, p0, dummy_number, index_name)
		{
			switch (index_name)
			{
			case "RemoteIP"
				IP = p1;
				break;
				
			case "RemotePort"
				Port = p1;
				break;
				
			default
				AppLog("Bad configuration for UI, unknown key ", index_name);
				break;
			}
		}
		GCreateEx("UI", "CUI", IP, Port);
		break;

	case "Global"
		foreach(p1, p0, dummy_number, index_name)
		{
			if (p1 == "String")
			{
				GCreate(index_name,p1);
			}
			else
			{
				GCreateEx(index_name, p1);
			}


		}
		break;

	case "LocationMapping"
		GCreateEx("LocationMapping", "DKMap");
		foreach(p1, p0, dummy_number, index_name)
		{
			DKAdd(:LocationMapping, index_name, p1[0], p1[1], ret);
			if (ret)
			{
				PrintLn("Cannot DKAdd ", p1);
			}
		}
		break;
		
	case "JM"
		Init_Lib("JM", "JM");
		
		foreach(p1, p0, dummy_number, index_name)
		{
			StrAssign(FileName, "Log/", index_name, "_%Y%M%D.txt");
			FirstLine = "";
			TimeStampFormat = "%T ";
			CloseName = ".log";
			MaxFileSize = 0;
			TimeOption = 3;
			LogLevel = 0;
			LocalIP = "0.0.0.0";
			LocalPort = 8888;
			License = "";
			Handler = "JM_Handler";
			Enable = 1;
			
			GCreateEx2(index_name, "JMS", "JM");
			foreach(p2, p1, dummy_number, item_name)
			{
				switch (item_name)
				{
				case "LocalIP"
					LocalIP = p2;
					break;
					
				case "LocalPort"
					LocalPort = p2;
					break;
					
				case "LogEnable"
					Enable = p2;
					break;
					
				case "FileName"
					FileName = p2;
					break;
					
				case "FirstLine"
					FirstLine = p2;
					break;
					
				case "TimeStamp"
					TimeStampFormat = p2;
					break;
					
				case "CloseName"
					CloseName = p2;
					break;
					
				case "LogLevel"
					LogLevel = p2;
					break;
				
				case "TimeOption"
					TimeOption = p2;
					break;
					
				case "MaxFileSize"
					MaxFileSize = p2;
					break;
					
				case "Handler"
					Handler = p2;
					break;
					
				case "License"
					License = p2;
					break;
				}
			}
			StartStationEx@JM(:index_name, index_name, LocalIP, LocalPort, Handler, License, Enable, FileName, TimeStampFormat, LogLevel, TimeOption, MaxFileSize, CloseName, ret);
		}
		
		break;

	case "KERNEL"
		IP = "127.0.0.1";
		Port = 3000;
		foreach(p1, p0, dummy_number, index_name)
		{
			switch (index_name)
			{
			case "RemoteIP"
				IP = p1;
				break;
				
			case "RemotePort"
				Port = p1;
				break;
				
			default
				AppLog("Bad configuration for UI, unknown key ", index_name);
				break;
			}
		}
		GCreateEx("KERNEL", "KERNELC", IP, Port);
		break;

	case "PLC"
		Init_Lib("QSock", "QSock");
		foreach(p1, p0, dummy_number, index_name)
		{
			GCreateEx(index_name, "CMitsubishi", index_name);
			Enable = 0;					//Log is not enable as default
			foreach(p2, p1, dummy_number, item_name)
			{	
				switch (item_name)
				{
				case "Type"
					:index_name.Type = p2;
					break;
					
				case "IP"
					:index_name.IP = p2;
					break;
					
				case "Port"
					:index_name.Port = p2;
					break;
					
				case "LocalIP"
					:index_name.LocalIP = p2;
					break;
					
				case "LocalPort"
					:index_name.LocalPort = p2;
					break;
					
				case "IO"
					:index_name.InitNode(p2);
					break;
					
				case "Log"
					Enable = 1;
					StrAssign(FileName, "Log/", index_name, "_%Y%M%D.txt");
					FirstLine = "";
					TimeStampFormat = "%T ";
					CloseName = ".log";
					MaxFileSize = 0;
					TimeOption = 3;
					LogLevel = 0;
					foreach(p3, p2, dummy_number, key3)
					{
						switch (key3)
						{	
						case "FileName"
							FileName = p3;
							break;
							
						case "TimeStampFormat"
							TimeStampFormat = p3;
							break;
							
						case "LogLevel"
							LogLevel = p3;
							break;
							
						case "MaxFileSize"
							MaxFileSize = p3;
							break;
							
						case "TimeOption"
							TimeOption = p3;
							break;
							
						case "CloseName"
							CloseName = p3;
							break;
							
						case "FirstLine"
							FirstLine = p3;
							break;
						}
					}
					break;
				}
			}
			if (Enable)
			{
				SetLogOption@QSock(:index_name.Sock, FileName, TimeStampFormat, LogLevel, MaxFileSize, TimeOption, CloseName, FirstLine);
				StartLog@QSock(:index_name.Sock);
			}
			
			PCall("StartLoop", index_name);
		}
	}
}

return;

AppLog("Initializing ", :EAP["ToolID"]);

if (IMapCheck(:EAP, "Global"))
{
	n = GetCount(:EAP["Global"]);
	loop = 0;
	while (loop < n)
	{
		s = IMapGetKey(:EAP["Global"], loop);
		GCreateEx(s, :EAP["Global"][loop]);
		Inc(loop);
	}
}

if (ScriptExist("DeveloperInit"))
{
	DeveloperInit();
}

if (IMapCheck(:EAP, "QSock"))
{
	Init_Lib("QSock", "QSock");	
	
	n = GetCount(:EAP["QSock"]);
	loop = 0;
	while (loop < n)
	{
		s = IMapGetKey(:EAP["QSock"], loop);
		GCreateEx2(s, "socket", "QSock");
		
		SetLogOption@QSock(:s, s, "%T", 0, ToU8(0), 3);
		StartLog@QSock(:s);

		if (IMapCheck(:EAP["QSock"][loop], "Type") == 0)
		{
			AppLogEx("Required type is not configured in QSock for ", s);
			throw;
		}
		
		switch (:EAP["QSock"][loop]["Type"])
		{
		case "TCP_Server"
			OpenSocket@QSock(:s, 2, 1, 0, ret);
			if (ret)
			{
				AppLogEx("Failed to open socket for ", s, ".  Error code ", ret);
				throw;
			}
			
			IP = "0.0.0.0";
			if (IMapCheck(:EAP["QSock"][loop], "LocalIP"))
			{
				IP = :EAP["QSock"][loop]["LocalIP"];
			}

			if (IMapCheck(:EAP["QSock"][loop], "LocalPort") == 0)
			{
				AppLogEx("Required Port not configured for QSock in ", s);
				throw;
			}

			Port = :EAP["QSock"][loop]["LocalPort"];
			BindIPv4@QSock(:s, IP, Port, 1, ret);
			if (ret)
			{
				AppLogEx("Failed to bind local port for ", s, ".  Error code ", ret);
				throw;
			}
			AppLogEx(s, " bind is succesful");
			
			SetBlocking@QSock(:s, 0, ret);
			if (ret)
			{
				AppLogEx("Failed to set to non-blocking mode for ", s, ".  Error code ", ret);
				throw;
			}
			
			Listen@QSock(:s, 1, ret);
			if (ret)
			{
				AppLogEx("Failed to listen for ", s, ".  Error code ", ret);
				throw;
			}
			
			PCall("AcceptOne", s);
			break;

		}
		
		Inc(loop);
	}
}

if (ScriptExist("DeveloperStart"))
{
	DeveloperStart();
}

EndScript

Script ShowG
(s, key, key2, key3)
Object o2;
U4 ticket,ret;

switch (GetCount(Parameter))
{
case 0
	PrintLn(Global);
	return;
	
case 1
	PrintLn(:s);

	return;
	
case 2
	PrintLn(:s[key]);
	return;
	
case 3
	Println(:s[key][key2]);
	return;
	
case 4
	Println(:s[key][key2][key3]);
	return;
}
EndScript 

Script ShowGCount
(s, key, key2, key3)

switch (GetCount(Parameter))
{
case 0
	PrintLn(GetCount(Global));
	return;
	
case 1
	PrintLn(GetCount(:s));
	return;
	
case 2
	PrintLn(GetCount(:s[key]));
	return;
	
case 3
	Println(GetCount(:s[key][key2]));
	return;
	
case 4
	Println(GetCount(:s[key][key2][key3]));
	return;
}
EndScript 

Script ShowGType
(s, key, key2, key3)

switch (GetCount(Parameter))
{
case 0
	PrintLn(GetType(Global));
	PrintLn(GetActualType(Global));
	return;
	
case 1
	PrintLn(GetType(:s));
	PrintLn(GetActualType(:s));
	return;
	
case 2
	PrintLn(GetType(:s[key]));
	PrintLn(GetActualType(:s[key]));
	return;
	
case 3
	Println(GetType(:s[key][key2]));
	Println(GetActualType(:s[key][key2]));
	return;
	
case 4
	Println(GetType(:s[key][key2][key3]));
	Println(GetActualType(:s[key][key2][key3]));
	return;
}
EndScript 

Script Init_SECS
(secs)

PrintLn(secs);

String s;
PString lp;
U0 n, loop, n2, loop2, n3, loop3;
U4 lret;
String ip;
String handler;
U4 port, mode, did, u4, logoption, roll;
U0 MaxFileSize;
String tstr, fline;
String tmp;
PString SArray;
U4 T3, T5, T6, T7, T8;

//CreateGlobalFlag to Indicate InitReportStatus.  
GCreate("InitReportRequired", 1);
GCreate("StopEAP", 0);

//Init the global objects
/*
n = GetCount(:EAP["GlobalObjects"]);
loop = 0;
while (loop < n)
{
	GCreateEx(IMapGetKey(:EAP["GlobalObjects"], loop), :EAP["GlobalObjects"][loop]);
	Inc(loop);
}
*/


//Start the SECS connection
n = GetCount(:EAP["SECS"]);
PrintLn(ScriptName(), "Total connection configure is ", n);

loop = 0;
while (loop < n)		//loop through the number of SECS Interface
{
	//Init the tool specific info, this is group under "SECS"
	//Read_JSON_File_Into_Global(:EAP["SECS"][loop]["Setup"], IMapGetKey(:EAP["SECS"], loop));
	
	s = IMapGetKey(secs, loop);
	PrintLn(ScriptName(), "Starting SECS connection for ", s);
	GCreateEx2(s, "HSMS", "QSecs");
	
	//mode = :EAP["SECS"][loop]["Connect mode"];
	mode = 1;
	SetConnectMode@QSecs(:s, mode);		//set the connection mode
	if (mode)		//this is active connection
	{
		ip = :EAP["SECS"][loop]["Remote IP"];
		port = :EAP["SECS"][loop]["Remote port"];	
		SetLocalIP@QSecs(:s, "0.0.0.0", 0);		//Bind to any/
		SetRemoteIP@QSecs(:s, ip, port);
	}
	else			//this is passive connection
	{
		ip = :EAP["SECS Connection"][loop]["Local IP"];
		port = :EAP["SECS Connection"][loop]["Local port"];	
		SetLocalIP@QSecs(:s, ip, port);			//Bind to specified by the configuration
	}
	
	did = :EAP["SECS"][loop]["Device ID"];
	SetDeviceID@QSecs(:s, did);
	
	handler = secs[loop]["Handler"];
	SetHandler@QSecs(:s, handler, 1);			//0 for parallel call, 1 for serial call
	SetName@QSecs(:s, s);
	//T3 = :EAP["SECS Connection"][loop]["SECS Timeout"]["T3"];
	//T5 = :EAP["SECS Connection"][loop]["SECS Timeout"]["T5"];
	//T6 = :EAP["SECS Connection"][loop]["SECS Timeout"]["T6"];
	//T7 = :EAP["SECS Connection"][loop]["SECS Timeout"]["T7"];
	//T8 = :EAP["SECS Connection"][loop]["SECS Timeout"]["T8"];
	//SetTimeOut@QSecs(:s, T3, T5, T6, T7, T8);

	MaxFileSize = 0;
	//u4 = :EAP["SECS Connection"][loop]["LogFile"]["Roll over"];
	//logoption = :EAP["SECS Connection"][loop]["LogFile"]["Option"];
	SetLogOption@QSecs(:s, "Log/SECS_%Y%M%D.log", "%T ", 13, MaxFileSize, 3);		//Use timestamp, and log everything, with MaxLogFile set, and no time roll over
	SetLogOn@QSecs(:s, 1);			//Enable the log
	
	Start@QSecs(:s);
	PrintLn("Secs Start");
	
	/*
	if (IMapCheck(:s, "DC"))		//if there is a DC plan
	{
		CreateDC(:s["DC"]);		//Create DC Plan for the SECS Interface
	}
	*/
	
	/*
	IMapPushEx(:s, "Extra_CEID", "DKMap");
	IMapPushEx(:s, "Extra_VID", "DKMap");
	IMapPushEx(:s, "Extra_Report", "DKMap");
	IMapPushEx(:s, "VID_LOOKUP", "Map");
	
	n2 = GetCount(:s["CEID"]);
	loop2 = 0;
	while (loop2 < n2)
	{
		DKAdd(:s["Extra_CEID"], IMapGetKey(:s["CEID"], loop2), :s["CEID"][loop2][0], :s["CEID"][loop2][1], lret);
		if (lret)
		{
			PrintLn(ScriptName(), "CEID name or ID not unique ", IMapGetKey(:s["CEID"], loop2));
			throw;		//CEID name or ID not unique
		}
		Inc(loop2);
	}
	
	n2 = GetCount(:s["VID"]);
	loop2 = 0;
	while (loop2 < n2)
	{
		DKAdd(:s["Extra_VID"], IMapGetKey(:s["VID"], loop2), :s["VID"][loop2][0], :s["VID"][loop2][1], lret);
		if (lret)
		{
			PrintLn(ScriptName(), "VID name or ID not unique ", IMapGetKey(:s["VID"], loop2));
			throw;		//VID name or ID not unique
		}
		Inc(loop2);
	}
	
	n2 = GetCount(:s["RPTID"]);
	loop2 = 0;
	while (loop2 < n2)
	{
		n3 = GetCount(:s["RPTID"][loop2]);
		if (n3 <= 1)
		{
			PrintLn(ScriptName(), s, " has RPTID problem at index ", loop2);
			throw;		//RPTID problem
		}
			
		Redim (SArray, n3 - 1);
		loop3 = 1;
		while (loop3 < n3)
		{
			SArray[loop3 - 1] = :s["RPTID"][loop2][loop3];
			Inc(loop3);
		}

		DKAdd(:s["Extra_Report"], IMapGetKey(:s["RPTID"], loop2), :s["RPTID"][loop2][0], SArray, lret);
		if (lret)
		{
			PrintLn(ScriptName(), "RPTID name or ID not unique ", IMapGetKey(:s["RPTID"], loop2));
			throw;		//RPTID name or ID not unique
		}
		Inc(loop2);
	}
	
	switch (:InitReportRequired)
	{
		case 1
			:InitReportRequired = 2;
			break;
			
		case 3
			:InitReportRequired = 0;
			PCall("InitReport", s);
			break;
	}
	*/
	
	Inc(loop);
}

PrintLn(ScriptName(), "Completed initialization");

EndScript

Script E84Log
String s;
U4 loop = 0, n;
n = GetCount(Parameter);
while (loop < n)
{
	StrAppend(s, Parameter[loop]);
	Inc(loop);
}
WriteLn@QLog(:E84Log, s);
EndScript

Script EventLog
String s;
U4 loop = 0, n;
n = GetCount(Parameter);
while (loop < n)
{
	StrAppend(s, Parameter[loop]);
	Inc(loop);
}
WriteLn@QLog(:EventLog, s);
EndScript

Script EventLogEx
(i)
String s;
U4 loop = 1, n;
n = GetCount(Parameter);
while (loop < n)
{
	StrAppend(s, Parameter[loop]);
	Inc(loop);
}
WriteLnEx@QLog(:EventLog, ToU4(i), s);
EndScript

Script AppLog
String s;
U4 loop = 0, n;
n = GetCount(Parameter);
while (loop < n)
{
	StrAppend(s, Parameter[loop]);
	Inc(loop);
}
WriteLn@QLog(:AppLog, s);
EndScript

Script AppLogEx
(i)
String s;
U4 loop = 1, n;
n = GetCount(Parameter);
while (loop < n)
{
	StrAppend(s, Parameter[loop]);
	Inc(loop);
}
WriteLnEx@QLog(:AppLog, ToU4(i), s);
EndScript

Script GLogLn
(name)
String s;
U4 loop = 1, n;
n = GetCount(Parameter);
while (loop < n)
{
	StrAppend(s, Parameter[loop]);
	Inc(loop);
}
WriteLn@QLog(:name, s);
EndScript

Script GLog
(name)
String s;
U4 loop = 1, n;
n = GetCount(Parameter);
while (loop < n)
{
	StrAppend(s, Parameter[loop]);
	Inc(loop);
}
Write@QLog(:name, s);
EndScript


Script SendToHTTP
(out, ip, port)

U4 ret;
U0 sent_len;
socket@QSock x;
U4 WriteInhibit = 1;
String s;
I4 event;
Vector v;

OpenSocket@QSock(x, 2, 1, 0, ret);
SetLogOption@QSock(x, "Log/HTTP_%Y%M%D.txt", "%T ",0,0,0);
StartLog@QSock(x);

BindIPv4@QSock(x, "0.0.0.0", 0, 1, ret);
if (ret)
{
	AppLogEx(ScriptName(), " - failed to bind to 0.0.0.0:0");
	return;
}

GetHostByName@QSock(ip, v);
if (GetCount(v))
{
	ConnectIPv4@QSock(x, v[0], port, ret);
	switch (ret)
	{
	case 10035
		WaitSockEvent@QSock(x, event, 5000, ret);
		AppLogEx("WSAWOULDBLOCK wait returns ", ret, ", event is ", event);
		break;
		
	case 0
		break;
		
	default
		AppLogEx("HTTP connect failed");
		return;
	}

	Send@QSock(x, out, sent_len, ret);
	if (ret || sent_len != StrLen(out))
	{
		AppLogEx(ScriptName(), " send failed");
		return;
	}

	XXX:
	WaitSockEvent@QSock(x, event, 5000, ret);
	if (ret)
	{
		AppLogEx(ScriptName(), " HTTP wait failure");
		return;
	}

	if (event & 1)
	{
		AppLogEx(ScriptName(), "Timeout waiting for response from HTTP server");
		return;
	}

	if (event & 4)			//read event
	{
		Receive@QSock(x, s, ToU0(1024), ret);			//receive 1024 bytes
		if (ret)
		{
			AppLogEx("Receive failed with ", ret);
			return;
		}
		if (StrLen(s))
		{
			AppLogEx("[HTTP] Server responded ",s);
			return;
		}
	}
	goto XXX;
}
else
{
	AppLogEx("Failed to resolve host name ", ip);
}

EndScript

Script StartLoop
(host)
:host.MessageLoop();
EndScript

Script GetJMResource
(rname, s)

U4 ret;
Object o;
GetResource(rname, s, ret);
if (ret)
{
	AppLog("Failed to GetJMResource", s);
	PrintLn("Failed to GetJMResource", s);
	return;
}
JSONParse(o, s, ret);
if (ret)
{
	AppLog("Failed to GetJMResource", s);
	PrintLn("Failed to GetJMResource", s);
	return;
}

EndScript

Script GetJSON
(rname, o)
U4 ret;
String s;
GetResource(rname, s, ret);
if (ret)
{
	AppLog("Failed to GetJMResource", rname);
	PrintLn("Failed to GetJMResource", rname);
	return;
}
JSONParse(o, s, ret);
if (ret)
{
	AppLog("Failed to GetJMResource", rname);
	PrintLn("Failed to GetJMResource", rname);
	return;
}
EndScript

Script ToJSON
(o)
String s;
U4 ret;
JSONWrite(o, 0, s, ret);
return s;
EndScript
