Script StrBinToDec
	(StrBinary)
	U2 Bin = 0;
	U2 StrLength = 0,i = 0,bit = 0,position = 0;
	U1 result = 0;
	StrLength = StrLen(StrBinary);
	while(i<=StrLength)
	{
		result = StrFind(StrBinary,"1",i,bit);
		if(result == 0)
		{
			wbreak;
		}
		i = bit+1;
		position = StrLength - bit -1;
		Bin = Bin + (1 << position);
	}
	return Bin;
EndScript

//call by reference 修改 call by value  2022/11/15 Alen
Script DecToStrBin  
	(quotient)
	U1 count = 15;
	I1 Fill = 0;
	PU2 Remainder;
	String Bin;
	Redim(Remainder,16);
	U2 temp;
	
	temp = quotient;

	if(temp == 0)
	{
		return "0000000000000000";
	}
	while(temp != 0)
	{
		Remainder[count] = temp % 2;
		temp = temp/2;
		Dec(count);
	}
	while(Fill < 16)
	{
        if(Fill < count)
        {
		    StrAppend(Bin,"0");
        }
        else
        {
            StrAppend(Bin,Remainder[count]);
            Inc(count);
        }
		Inc(Fill);
	}
	return Bin; 
EndScript

Script CSstate
    CV cv;
    U4 Status = 1;
EndScript

Script CSC
    CV cv;
    Map CSlist;



    Script lock
        (name)
        I2 ret = 0;
        if(MapCheck(CSlist, name))
        {
            if(CSlist[name].Status)
            {
                if(wait(name))
                {
                    CSlist[name].Status = 1;
                    ret = 1;
                }
                else
                {
                    PrintLn(name, " wait CS lock Time out!!!!");
                    ret = 0;
                }
            }
            else
            {
                CSlist[name].Status = 1;
                ret = 1;
            }
        }
        else
        {
            MapAddEx(CSlist, name, "CSstate");
            ret = 1;
        }
        return ret;
    EndScript

    Script wait
        (name)
        U2 Timer = 0;
        while( Timer < 100)
        {
            if(!CSlist[name].Status)
            {
                return 1;
            }
            Inc(Timer);
            Sleep(100);
        }
        return 0;

    EndScript

    Script stop
        (name)
        I2 ret = 0;
        if(MapCheck(CSlist, name))
        {
            CSlist[name].Status = 0;
            ret = 1;
        }
        else
        {
            PrintLn(name, " wait CS lock Time out!!!!");
            ret = 0;
        }
        return ret;
    EndScript

EndScript

Script lockBit
	(reqWord,reqNum,bit)
	I1 result = 0;
	:CS.lock(reqWord);
	result = :EFEM.SetBitinWord(reqWord,reqNum,bit);
	:CS.stop(reqWord);
	return result;
EndScript

Script lockContinuousBit
	(reqWord,reqNum,Strbit)
	I1 result = 0;
	:CS.lock(reqWord);
	result = :EFEM.ContinuousSetBitinWord(reqWord,reqNum,Strbit);
	:CS.stop(reqWord);
	return result;
EndScript

Script lockWord //WriteWord_3E
	(reqWord, value, ret)
	I2 retv = 0;
	:CS.lock(reqWord);
	:EFEM.WriteWord_3E(reqWord, value, ret);
	retv = ret;
	:CS.stop(reqWord);
	return retv;
EndScript

Script lockContinuousWord //WriteWordConsecutive_3E
	(reqWord, value, ret)
	I2 retv = 0;
	:CS.lock(reqWord);
	:EFEM.WriteWordConsecutive_3E(reqWord, value, ret);
	retv = ret;
	:CS.stop(reqWord);
	return retv;
EndScript

Script lockPrimaryHs
	(reqWord,  reqNum, replyWord, replyNum, t1, t2)
	I1 result = 0;
	:CS.lock(reqWord);
	result = :EFEM.PrimaryHandShakeWord(reqWord,  reqNum, replyWord, replyNum, t1, t2);
	:CS.stop(reqWord);
	return result;
EndScript

Script lockSecondaryHs
	(reqWord,  reqNum, replyWord, replyNum, t1)
	I1 result = 0;
	:CS.lock(replyWord);
	result = :EFEM.SecondaryHandShakeWord(reqWord,  reqNum, replyWord, replyNum, t1);
	:CS.stop(replyWord);
	return result;
EndScript