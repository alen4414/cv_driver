Resource SAMPLE_R
{	
    "TxName":"SAMPLE_R",
    "TxID":1,
    "ID":"CV_Driver",
    "TimeStamp":"202205061417222",
    "Content":
    {
        "ACK":0
    }
}
EndResource

Resource IOPORTSTATUS_E
{	
    "TxName":"IOPORTSTATUS_E",
    "TxID":1,
    "ID":"CV_Driver",
    "TimeStamp":"202205061417222",
    "Content":
    {
        "PORTNO":1,
        "STATUS":"INSERVICE",
        "DIRECTION":"INMODE",
        "REQUEST":"NOREQUEST"
    }
}
EndResource

Resource IOPORTPRESENCECHANGE_E
{	
    "TxName":"IOPORTPRESENCECHANGE_E",
    "TxID":1,
    "ID":"CV_Driver",
    "TimeStamp":"202205061417222",
    "Content":
    {
        "PORTNO": 1,
        "STAGE" : 1,
        "PRESENCE" : "OFF",
        "CARRIERID" : "NOREQUEST"
    }
}
EndResource 

Resource IOPORTREADER_E
{
    "TxName": "IOPORTREADER_E",
    "TxID": 0,
    "ID": "CV_Driver",
    "TimeStamp": "202205171032222",
    "Content": {
        "PORTNO": 1,
        "CARRIERID" : ""
    }
}
EndResource 

Resource QUERYIOPORTDIRECTION_E
{
    "TxName": "QUERYIOPORTDIRECTION_E",
    "TxID": 0,
    "ID": "CV_Driver",
    "TimeStamp": "202205171032222",
    "Content": {
        "PORTNO": 1,
        "DIRECTION" : "INMODE"
    }
}
EndResource 


Resource E84PASSIVE_E
{
    "TxName":"E84PASSIVE_E",
    "TxID": 0,
    "ID": "CV_Driver",
    "TimeStamp": "202205171032222",
    "Content": {
        "PORTNO": 1,
        "VS_1": 0,
        "VS_0": 0,
        "VA": 0,
        "ES": 0,
        "HO_AVBL": 0,
        "U_REQ": 0,
        "L_REQ": 0,
        "READY":0
    }
}
EndResource 