Script CMitsubishi
(name)

	String Name = name;
	String Type = "TCP";
	String IP = "127.0.0.1", LocalIP = "0.0.0.0";
	U4 Port = 5999, LocalPort = 0;
	U4 SockState = 0;
	U4 event;
	U4 Command;			//this is a flag to indicate there is something to be sent
	socket@QSock Sock;
	
	U4 ErrorCount = 0;

	Map mapPlan;			//plan name to 3E message
	Map mapNode;			//node name to address

	String Out, In;
	U4 Ret, Timer = 5000;
	CV cv, cvHard;

	Script InitNode
	(fname)

		Reference p, X;

		Object o;
		U0 index;
		String key, s, tmp;
		U4 ret;

		if (StrReadFile(s, fname))
		{
			PrintLn("Failed to read file ", fname);
			throw;
		}

		JSONParse(o, s, ret);

		if (ret)
		{
			PrintLn("Failed to parse JSON at offset ", ret, " for ", fname);
			throw;
		}

		foreach(p, o, index, key)
		{
			if (GetAddress(p, s))
			{
				PrintLn("Failed to GetAddress ", p, " for ", key, " in ", Name); 
				throw;
				
			}
			MapAdd(mapNode, key, s);
		}
		
	EndScript

	Script GetAddress
	(p, s)
	
	//return -1 if failed
	//return 0 if OK

	String M;
	U4 RegionLen;
	String tmp, tmp2;

		switch (StrSub(p, 0, 1))
		{
		case "B"
			M = "\xA0";
			RegionLen = 1;
			break;
			
		case "C"		//could be CC, CN, CS
			switch (StrSub(p, 1, 1))
			{
			case "S"
				M = "\xC4";
				RegionLen = 2;
				break;
				
			case "C"
				M = "\xC3";
				RegionLen = 2;
				break;
				
			case "N"
				M = "\xC5";
				RegionLen = 2;
				break;
				
			default
				return -1;
			}
			break;
			
		case "D"
			switch (StrSub(p, 1, 1))		//could be D, DX, DY
			{
			case "X"
				M = "\xA2";
				RegionLen = 2;
				break;
				
			case "Y"
				M = "\xA3";
				RegionLen = 2;
				break;
				
			default
				M = "\xA8";
				RegionLen = 1;
				break;
			}
			break;
			
		case "F"
			M = "\x93";
			RegionLen = 1;
			break;
			
		case "L"
			M = "\x92";
			RegionLen = 1;
			break;
			
		case "M"
			M = "\x90";
			RegionLen = 1;
			break;

		case "R"
			M = "\xAF";
			RegionLen = 1;
			break;
			
		case "S"		//could be SB, SD, SM, SW
			switch (StrSub(p, 1, 1))
			{
			case "B"
				M = "\xA1";
				RegionLen = 2;
				break;
				
			case "D"
				M = "\xA9";
				RegionLen = 2;
				break;

			case "M"
				M = "\x91";
				RegionLen = 2;
				break;
				
			case "W"
				M = "\xB5";
				RegionLen = 2;
				break;
				
			case "T"		//could be STS, STC, STN
				switch (StrSub(p, 2, 1))
				{
				case "S"
					M = "\xC7";
					RegionLen = 3;
					break;
					
				case "C"
					M = "\xC6";
					RegionLen = 3;
					break;
					
				case "N"
					M = "\xC8";
					RegionLen = 3;
					break;
					
				default
					return -1;	
				}
				break;
				
			default
				return -1;
			}
			break;
			
		case "T"
			switch (StrSub(p, 1, 1))
			{
			case "S"
				M = "\xC1";
				RegionLen = 2;
				break;
				
			case "C"
				M = "\xC0";
				RegionLen = 2;
				break;
				
			case "N"
				M = "\xC2";
				RegionLen = 2;
				break;
				
			default
				return -1;
			}
			break;
			
		case "V"
			M = "\x94";
			RegionLen = 1;
			break;
			
		case "W"
			M = "\xB4";
			RegionLen = 1;
			break;
			
		case "X"
			M = "\x9C";
			RegionLen = 1;
			break;
			
		case "Y"
			M = "\x9D";
			RegionLen = 1;
			break;
			
		case "Z"
			if (StrSub(p, 1, 1) == "R")
			{
				M = "\xB0";
				RegionLen = 2;
			}
			else
			{
				M = "\xCC";
				RegionLen = 1;
			}
			break;
			
		default
			return -1;
		}
			
		if (StrLen(p) - RegionLen > 6)
		{
			return -1;
		}
		
		StrAssign(tmp2, StrRepeat(tmp, "0", 6 + RegionLen - StrLen(p)), StrSub(p, RegionLen, -1));
		StrAssign(s, StrEndian(Hex2U8(tmp2), 3, 0), M);
		
		return 0;
		
	EndScript
	
	Script WriteNodeHS
	(node, v, reqBit, replyBit, t1, t2)
		U4 ret;
		
		WriteWord_3E(node, v, ret);
		if (ret)
		{
			return 0;
		}
		
		switch (GetCount(Parameter))
		{
		case 4
			return PrimaryHandShake(reqBit, replyBit);
			
		case 5
			return PrimaryHandShake(reqBit, replyBit, t1);
			
		default
			return PrimaryHandShake(reqBit, replyBit, t2);
		}
		
	EndScript

	Script PrimaryHandShake
	(reqBit, replyBit, t1, t2, flag)
	
		//this function return 1 if HandShake is success.  Return 0 if fail
	
		//t1 and t2 are option and will be set to default of 10 (that means 1 second)
		//t1 is the number of cycle
		//t2 is the number of cycle to wait for replyBit to turn 0
		
		U4 ret;
		U4 rValue = 0;
		U4 t;
		U4 U4Flag = 0;

		WriteBit_3E(reqBit, 1, ret);					//write 1 to reqBit
		if (ret)
		{
			AppLog("Failed to set ", reqBit);
			return rValue;
		}
		
		if (GetCount(Parameter) >= 3)
		{
			t = Parameter[2];
		}
		else
		{
			t = 10;
		}
		
		if (GetCount(Parameter) == 5)
		{
			U4Flag = 1;
		}
		
		if (MonitorBitEx(replyBit, 1, t, U4Flag))				
		{
			WriteBit_3E(reqBit, 0, ret);
			
			if (GetCount(Parameter) >= 4)
			{
				t = Parameter[3];
			}
			else
			{
				t = 10;
			}
			
			if (GetCount(Parameter) == 5)
			{
				if (MonitorBitEx(replyBit, 0, t, U4Flag))
				{
					rValue = 1;
				}
			}
			else
			{			
				if (MonitorBitEx(replyBit, 0, t))
				{
					rValue = 1;
				}
			}
		}
		else
		{
			AppLog("Timeout waiting for ", replyBit, " to be set");
		}
		
		WriteBit_3E(reqBit, 0, ret);			//cancel the request
		if (ret)
		{
			AppLog("Failed to reset ", reqBit);
		}

	return rValue;
	EndScript

	Script SecondaryHandShake
	(bitReq, bitReply, t1)

		U4 rValue = 0;		//return value
		U4 ret;
		U4 t;

		WriteBit_3E(bitReply, 1, ret);
		if (ret)
		{
			AppLog("Failed to write to ", bitReply);
		}
		else
		{
			if (GetCount(Parameter) >= 3)
			{
				t = Parameter[2];
			}
			else
			{
				t = 10;
			}
			
			if (MonitorBitEx(bitReq, 0, t))		//expect value to go to 0, and about 1 second to time out
			{
				rValue = 1;
			}
			else
			{
				AppLog("Timeout waiting for ", bitReq, "to be reset");
			}
		}
		
		WriteBit_3E(bitReply, 0, ret);
		if (ret)
		{
			AppLog("Failure in resetting ", bitReply);
		}

	return rValue;

	EndScript

	Script MonitorBit
	(node, exp)
	//monitor node for the expected value exp
		
		U1 value;
		U4 ret;

		Again:
		ReadBit_3E(node, value, ret);
		if (ret)
		{
			AppLog("Failed to read ", node);
			AppLog(StackInfo());
		}
		else
		{
			if (exp && value || (exp == 0 && value == 0))
			{
				return 1;
			}
		}

		if (SleepEx(100))
		{
			return 0;
		}
		
		goto Again;

	EndScript

	Script MonitorBitEx
		(node, exp, time, flag)
		
		
		U1 value;
		U4 ret;

		while (time)
		{
			Dec(time);
			ReadBit_3E(node, value, ret);
			if (ret)
			{
				AppLog("Failed to read ", node);
				AppLog(StackInfo());
			}
			else
			{
				if (exp && value || (exp == 0 && value == 0))
				{
					if (GetCount(Parameter) == 4 && flag)
					{
						PrintLn("Remaining time is ", time);
					}
					return 1;
				}	
			}
			if (SleepEx(100))
			{
				return 0;
			}
		}
		return 0;
	EndScript

	Script PrimaryHandShakeMulti
	(reqBit, replyBit, replyBit2, t1, t2, flag)
	
		//this function return 1 if HandShake is success.  Return 0 if fail
	
		//t1 and t2 are option and will be set to default of 10 (that means 1 second)
		//t1 is the number of cycle
		//t2 is the number of cycle to wait for replyBit to turn 0
		
		U4 ret;
		U4 rValue = 0;
		U4 t;
		U4 U4Flag = 0;

		WriteBit_3E(reqBit, 1, ret);					//write 1 to reqBit
		if (ret)
		{
			AppLog("Failed to set ", reqBit);
			return rValue;
		}
		
		if (GetCount(Parameter) >= 4)
		{
			t = Parameter[3];
		}
		else
		{
			t = 10;
		}
		
		if (GetCount(Parameter) == 6)
		{
			U4Flag = 1;
		}
		
		switch(MonitorMultiBitEx(replyBit, replyBit2, 1, t, U4Flag))				
		{
			case 1
				WriteBit_3E(reqBit, 0, ret);
				if (GetCount(Parameter) >= 5)
				{
					t = Parameter[4];
				}
				else
				{
					t = 10;
				}
				
				if (GetCount(Parameter) == 6)
				{
					if (MonitorBitEx(replyBit, 0, t, U4Flag))
					{
						rValue = 1;
					}
				}
				else
				{			
					if (MonitorBitEx(replyBit, 0, t))
					{
						rValue = 1;
					}
				}
			break;

			case 2
				WriteBit_3E(reqBit, 0, ret);
				if (GetCount(Parameter) >= 5)
				{
					t = Parameter[4];
				}
				else
				{
					t = 10;
				}
				
				if (GetCount(Parameter) == 6)
				{
					if (MonitorBitEx(replyBit2, 0, t, U4Flag))
					{
						rValue = 2;
					}
				}
				else
				{			
					if (MonitorBitEx(replyBit2, 0, t))
					{
						rValue = 2;
					}
				}
			break;
			
			default
				AppLog("Timeout waiting for ", replyBit, " to be set");
			break;
		}

		WriteBit_3E(reqBit, 0, ret);			//cancel the request
		if (ret)
		{
			AppLog("Failed to reset ", reqBit);
		}

	return rValue;
	EndScript

	Script MonitorMultiBit
		(node, node2, exp)
		//monitor node for the expected value exp
			
			U1 value, value2;
			U4 ret, ret2;
	
			Again:
			ReadBit_3E(node, value, ret);
			if (ret)
			{
				AppLog("Failed to read ", node);
				AppLog(StackInfo());
			}
			else
			{
				if (exp && value || (exp == 0 && value == 0))
				{
					return 1;
				}
			}

			ReadBit_3E(node2, value2, ret2);
			if (ret2)
			{
				AppLog("Failed to read ", node2);
				AppLog(StackInfo());
			}
			else
			{
				if (exp && value2 || (exp == 0 && value2 == 0))
				{
					return 2;
				}
			}
	
			if (SleepEx(100))
			{
				return 0;
			}
			
			goto Again;
	
		EndScript
		Script MonitorMultiBitEx
			(node, node2, exp, time, flag)
			
			
			U1 value, value2;
			U4 ret, ret2;
	
			while (time)
			{
				Dec(time);
				ReadBit_3E(node, value, ret);
				if (ret)
				{
					AppLog("Failed to read ", node);
					AppLog(StackInfo());
				}
				else
				{
					if (exp && value || (exp == 0 && value == 0))
					{
						if (GetCount(Parameter) == 5 && flag)
						{
							PrintLn("Remaining time is ", time);
						}
						return 1;
					}	
				}
				ReadBit_3E(node2, value2, ret2);
				if (ret2)
				{
					AppLog("Failed to read ", node2);
					AppLog(StackInfo());
				}
				else
				{
					if (exp && value2 || (exp == 0 && value2 == 0))
					{
						if (GetCount(Parameter) == 5 && flag)
						{
							PrintLn("Remaining time is ", time);
						}
						return 2;
					}	
				}
				if (SleepEx(100))
				{
					return 0;
				}
			}
			return 0;
		EndScript
	///////////////////////////////////////////////
	Script PrimaryHandShakeWord
	(reqWord,  reqNum, replyWord, replyNum, t1, t2)
	
		//this function return 1 if HandShake is success.  Return 0 if fail
	
		//t1 and t2 are option and will be set to default of 10 (that means 1 second)
		//t1 is the number of cycle
		//t2 is the number of cycle to wait for replyBit to turn 0
		//req and reply Num are point of the world(use 0~15)
		
		U4 ret;
		I2 rValue = 0;
		U4 t;
		U4 U4Flag = 0;
		U2 reqmask = 1;
		U2 WOriValue = 0,WNewValue = 0;

		if (GetCount(Parameter) == 7) //shake time flag
		{
			U4Flag = 1;
		}

		reqmask = reqmask << ToU2(reqNum);
		ReadWord_3E(reqWord, WOriValue, ret);
		if(ret)
		{
			AppLog("Failed to get ", reqWord);
			rValue = -1;
			return rValue;
		}
		WNewValue = WOriValue | reqmask;
		WriteWord_3E(reqWord, WNewValue, ret);					
		if (ret)
		{
			AppLog("Failed to set ", reqWord);
			rValue = -1;
			return rValue;
		}
		
		if (GetCount(Parameter) >= 5)
		{
			t = Parameter[4];
		}
		else
		{
			t = 30;
		}
		
		if (MonitorWordEx(replyWord, replyNum, 1, t, U4Flag))				
		{
			ReadWord_3E(reqWord, WOriValue, ret);
			if(ret)
			{
				AppLog("Failed to get ", reqWord);
				rValue = -1;
				return rValue;
			}

			WNewValue = WOriValue & ~reqmask;

			WriteWord_3E(reqWord, WNewValue, ret);
			if (ret)
			{
				AppLog("Failed to set ", reqWord);
				rValue = -1;
				return rValue;
			}

			if (GetCount(Parameter) >= 6)
			{
				t = Parameter[5];
			}
			else
			{
				t = 30;
			}
			
			if (MonitorWordEx(replyWord, replyNum, 0, t, U4Flag))
			{
				rValue = 1;
			}
			else
			{
				rValue = -3; //Second HandShake didn't set 0.
			}
		}
		else
		{
			AppLog("Timeout waiting for ", replyWord, " to be set");
			rValue = -2; //Frist HandShake didn't set 1.
		}
		
		//Time Out cancel the request
		ReadWord_3E(reqWord, WOriValue, ret);
		if(ret)
		{
			AppLog("Failed to get ", reqWord);
			rValue = -1;
			return rValue;
		}

		WNewValue = WOriValue & ~reqmask;

		WriteWord_3E(reqWord, WNewValue, ret);
		if (ret)
		{
			AppLog("Failed to set ", reqWord);
			rValue = -1;
			return rValue;
		}


	return rValue;
	EndScript

	Script SecondaryHandShakeWord
		(reqWord,  reqNum, replyWord, replyNum, t1)


		I2 rValue = 0;		//return value
		U4 ret;
		U4 U4Flag = 0;
		U4 t;
		U2 WOriValue=0, WNewValue=0;
		U2 replymask =1;

		if (GetCount(Parameter) == 7) //shake time flag
		{
			U4Flag = 1;
		}

		replymask = replymask << ToU2(replyNum);
		ReadWord_3E(replyWord, WOriValue, ret);
		if(ret)
		{
			AppLog("Failed to get ", replyWord);
			rValue = -1;
			return rValue;
		}

		WNewValue = WOriValue | replymask;

		WriteWord_3E(replyWord, WNewValue, ret);
		if (ret)
		{
			AppLog("Failed to set ", replyWord);
			rValue = -1;
			return rValue;
		}
		else
		{
			if (GetCount(Parameter) >= 5)
			{
				t = Parameter[4];
			}
			else
			{
				t = 30;
			}
			
			if (MonitorWordEx(reqWord, reqNum, 0, t, U4Flag))		//expect value to go to 0, and about 1 second to time out
			{
				rValue = 1;
			}
			else
			{
				AppLog("Timeout waiting for ", reqWord, "to be reset");
				rValue = -3; //Second HandShake didn't set 0.
			}
		}
		
		//Time Out cancel the request
		ReadWord_3E(replyWord, WOriValue, ret);
		if(ret)
		{
			AppLog("Failed to get ", replyWord);
			rValue = -1;
			return rValue;
		}

		WNewValue = WOriValue & ~replymask;

		WriteWord_3E(replyWord, WNewValue, ret);
		if (ret)
		{
			AppLog("Failed to set ", replyWord);
			rValue = -1;
			return rValue;
		}

			return rValue;

	EndScript

	Script MonitorWord
	(Word, WordNum, exp)
	//monitor node for the expected value exp 0 or 1.
		
		U2 value;
		U4 ret;
		U2 Wordmask = 1;

		Wordmask = Wordmask << ToU2(WordNum);
		Again:
		ReadWord_3E(Word, value, ret);
		if (ret)
		{
			AppLog("Failed to read ", Word);
			AppLog(StackInfo());
		}
		else
		{
			value = value & Wordmask;
			if (exp && value || (exp == 0 && value == 0))
			{
				return 1;
			}
		}

		if (SleepEx(100))
		{
			return 0;
		}
		
		goto Again;

	EndScript

	Script MonitorWordEx
		(Word, WordNum, exp, time, flag)
		
		
		U2 value;
		U4 ret;
		U2 Wordmask = 1;

		Wordmask = Wordmask << ToU2(WordNum);
		while (time)
		{
			Dec(time);
			ReadWord_3E(Word, value, ret);
			if (ret)
			{
				AppLog("Failed to read ", Word);
				AppLog(StackInfo());
			}
			else
			{
				value = value & Wordmask;
				if (exp && value || (exp == 0 && value == 0))
				{
					if (GetCount(Parameter) == 5 && flag)
					{
						PrintLn("Remaining time is ", time);
					}
					return 1;
				}	
			}
			if (SleepEx(100))
			{
				return 0;
			}
		}
		return 0;
	EndScript

	Script SetBitinWord											//設定一個 Word 之中的其中一個 Bit。
		(reqWord,reqNum,bit)									// return -1 Error
		I1 ret;													// return 0  Success
		U2 reqmask = 1;											// (0) = SetBitinWord("W1000",0,1);
		U2 WOriValue = 0,WNewValue = 0;							//將 W1000.0 設為 1。

		reqmask = reqmask << ToU2(reqNum);
		ReadWord_3E(reqWord, WOriValue, ret);
		if(ret)
		{
			return -1;
		}

		if(ToU2(bit))
		{
			WNewValue = WOriValue | reqmask;
		}
		else
		{
			WNewValue = WOriValue & ~reqmask;
		}
		WriteWord_3E(reqWord, WNewValue, ret);
		if(ret)
		{
			return -1;
		}
		return 0;
	EndScript

	Script ContinuousSetBitinWord								//設定一個 Word 之中的其中一連串 Bit。
		(reqWord,reqNum,Strbit)									// return -1 Error
		I1 ret;													// return 0  Success
		U1 i = 0;												// (0) = ContinuousSetBitinWord("W1000",0,1001);
		U2 bit,Num1 = 1;										//將 W1000.0-3 設為 1001。
		U2 reqmask = 0;
		U2 WOriValue = 0;

		bit = StrBinToDec(Strbit);
		bit = bit << ToU2(reqNum); // change Strbit to Dec 
		while(i < StrLen(Strbit))
		{
			reqmask = reqmask + Num1; //create a whole 1 mask for length same as bit
			Num1 = Num1 << 1;
			Inc(i);
		}
		reqmask = reqmask << ToU2(reqNum); // shift mask to request postion
		ReadWord_3E(reqWord, WOriValue, ret);
		WOriValue = WOriValue & ~reqmask; //clean up request position bit
		WOriValue = WOriValue | bit; // change request position bit value to Strbit
		
		WriteWord_3E(reqWord, WOriValue, ret);
		if(ret == -1)
		{
			return -1;
		}
		return 0;
	EndScript
	
	Script GetBitinWord											//讀取一個 Word 之中的一個 Bit。
		(reqWord,reqNum)										// return -1 Error
		I1 ret;													// return 0/1 Bit value
		U2 reqmask = 1;											// (0) = GetBitinWord("W1000",0);
		U2 WOriValue = 0;										//讀取 W1000.0 其值為 0。
		reqmask = reqmask << ToU2(reqNum);
		ReadWord_3E(reqWord, WOriValue, ret);
		if(ret)
		{
			return -1;
		}
		if((WOriValue & reqmask) == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	EndScript

	Script ContinuousGetBitinWord								//讀取一個 Word 之中的一連串 Bit。
		(reqWord,reqNum,count)									// return = -1 Error
		U1 i = 1;												// return value
		I1 ret;													// (408) = ContinuousGetBitinWord("W1000",3,6);
		U2 WOriValue = 0,reqmask = 1,Num1;						//讀取 W1000.3-8，總共 6 個 Bit，
		reqmask = reqmask << ToU2(reqNum);						//W1000.3-8 Bit 換為 二進制為 0000 0001 1001 1000，十進制為 408。
		Num1 = reqmask;
		while(i<count)
		{
			Num1 = (Num1*2);
			reqmask = reqmask + Num1;
			i = i + 1;
		}
		:EFEM.ReadWord_3E(reqWord, WOriValue, ret);
		if(ret)
		{
			return -1;
		}
		return (WOriValue & reqmask);
	EndScript
//////////////////////////////////////////////
	Script ReadWord_3E
		(s, value, ret)

		String v;
		ReadWordConsecutive_3E(s, v, 1, ret);
		if (ret == 0)
		{
			BinaryAssign(value, v);
		}

	EndScript

	Script WriteWord_3E
		(s, value, ret)

		String v;
		BinaryAssign(v, ToU2(value));
		WriteWordConsecutive_3E(s, v, ret);

	EndScript
	
	Script SetupPlan
		(v, planname)
		U4 n = GetCount(v);
		U4 loop = 0;
		String tmp, out;
		U1 tlen = n;
		U2 outlength = n * 4 + 8;
		
		StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00", ToStream(outlength), "\x08\x00\x03\x04\x00\x00", ToStream(tlen), ToStream(0U1));
		
		while (loop < n)
		{
			if (MapCheck(mapNode, v[loop]))
			{
				tmp = mapNode[v[loop]];
			}
			else
			{
				if (GetAddress(v[loop], tmp))
				{
					AppLog("Unknown node ", v[loop], " specified in ", Name);
					return -1;
				}
			}
			StrAppend(out, tmp);
			Inc(loop);
		}
		MapAdd(mapPlan, planname, out);
		return 0;
		
	EndScript
	
	Script ReadPlan
		(planname, v, option, ret)
		
		//option is 0 if can directly BinaryAssign into v
		//option is 1 if v is PU2 and need to redim before binary assign
		
		String out, in;
		U4 lret;
		U1 n;
		U2 tlen;
		ret = -1;
		
		if (MapCheck(mapPlan, planname))
		{
			out = mapPlan[planname];
		}
		else
		{
			return;
		}
		
		SendAndWait(out, in, lret);

		if (lret == 0)
		{
			BinaryAssign(n, StrSub(out, 15, 1));
			tlen = n * 2 + 2;
			StrAssign(out, "\xD0\x00\x00\xFF\xFF\x03\x00", ToStream(tlen), "\x00\x00");			//recycle the variable out
			if (StrLen(in) == tlen + 9 && StrCmp(StrSub(in, 0, 11), out) == 0)
			{
				ret = 0;
				if (option)
				{
					Redim(v, n);
				}
				BinaryAssign(v, StrSub(in, 11, n * 2));
			}
		}
	
	EndScript
	
	
	Script ReadWordMulti_3E
		(v, value, ret)
		
		U4 loop = 0;
		U2 n = GetCount(v);
		U2 inlen = n * 2 + 2;		//2 response code, and 2 bytes for each note data
		U2 outlen = n * 4 + 8;		//2 timeout, 2 command, 2 sub command, 1 word count, 1 dword count, and 4 bytes in each node address
		String in, out;
		String tmp;			//to hold the 4 bytes 3E node address
		U4 lret;
		U1 u1 = n;
		
		ret = -1;
		StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00", ToStream(outlen), "\x08\x00\x03\x04\x00\x00", ToStream(u1), "\x00");
		while (loop < n)
		{
			if (MapCheck(mapNode, v[loop]))
			{
				tmp = mapNode[v[loop]];
			}
			else
			{
				if (GetAddress(v[loop], tmp))
				{
					AppLog("Unknown node ", v[loop], " specified in ", Name);
					return;
				}
			}
			StrAppend(out, tmp);
			Inc(loop);
		}
		
		SendAndWait(out, in, lret);

		if (lret == 0)
		{
			StrAssign(tmp, "\xD0\x00\x00\xFF\xFF\x03\x00", ToStream(inlen), "\x00\x00");
			if (StrLen(in) == inlen + 9 && StrCmp(StrSub(in, 0, 11), tmp) == 0)
			{
				ret = 0;
				BinaryAssign(value, StrSub(in, 11, n * 2));
			}
		}
		
	EndScript

	Script ReadWordConsecutive_3E
		(s, value, count, ret)

		String tmp;
		String out, in;
		U2 nlen = count * 2 + 2;		//nlen is the U2 in the 3E enconding for the 
		U2 n = count;					//n is the number of node
		U4 lret;

		ret = -1;
		if (MapCheck(mapNode, s))
		{
			tmp = mapNode[s];
		}
		else
		{
			if (GetAddress(s, tmp))
			{
				AppLog("Unknown node ", s, " specified in ", Name);
				return;
			}
		}

		StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00\x0C\x00\x08\x00\x01\x04\x00\x00", tmp, ToStream(n));

		SendAndWait(out, in, lret);

		if (lret == 0)
		{
			StrAssign(tmp, "\xD0\x00\x00\xFF\xFF\x03\x00", ToStream(nlen), "\x00\x00");
			if (StrLen(in) == nlen + 9 && StrCmp(StrSub(in, 0, 11), tmp) == 0)
			{
				ret = 0;
				BinaryAssign(value, StrSub(in, 11, n * 2));
			}
		}

	EndScript

	Script WriteWordConsecutive_3E
		(s, value, ret)

		String tmp;
		String out, in;
		U2 n, nlen;
		U4 lret;
		String tout;
		
		ret = -1;
		
		if (MapCheck(mapNode, s))
		{
			tmp = mapNode[s];
		}
		else
		{
			if (GetAddress(s, tmp))
			{
				AppLog("Unknown node ", s, " specified in ", Name);
				return;
			}
		}
		
		switch (GetType(value))
		{
		case "PU2"
			n = GetCount(value);
			nlen = n * 2 + 12;
			BinaryAssign(tout, value);
			StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00", ToStream(nlen), "\x08\x00\x01\x14\x00\x00", tmp, ToStream(n), tout);
			break;
			
		case "String"
			if (StrLen(value) % 2)
			{
				AppLog("Write length must be multiple of 2");
				PrintLn("Write length must be multiple of 2");
				throw;
			}
			n = StrLen(value) / 2;			//n is the number of node
			nlen = StrLen(value) + 12;
			StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00", ToStream(nlen), "\x08\x00\x01\x14\x00\x00", tmp, ToStream(n), value);
			break;
		}
		
		SendAndWait(out, in, lret);

		if ((lret == 0) && (StrLen(in) == 11) && (StrCmp(in, "\xD0\x00\x00\xFF\xFF\x03\x00\x02\x00\x00\x00") == 0))
		{
			ret = 0;
		}

	EndScript

	Script ReadBit_3E
	(s, value, ret)

		String tmp;
		String out, in;
		U1 x;
		U4 lret;

		ret = -1;
		if (MapCheck(mapNode, s))
		{	
			tmp = mapNode[s];
		}
		else
		{
			if (GetAddress(s, tmp))
			{
				AppLog("Unknown node ", s, " specified in ", Name);
				return;
			}
		}
		
		StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00\x0C\x00\x08\x00\x01\x04\x01\x00", tmp, "\x01\x00");
		SendAndWait(out, in, lret);

		if (lret == 0 && StrLen(in) == 12 && StrCmp(StrSub(in, 0, 11), "\xD0\x00\x00\xFF\xFF\x03\x00\x03\x00\x00\x00") == 0)
		{
			BinaryAssign(x, StrSub(in, 11, 1));
			if (x)
			{
				value = 1;
			}
			else
			{
				value = 0;
			}
			ret = 0;
		}		

	EndScript
	

	Script WriteBit_3E
	(s, value, ret)

		String tmp;
		String out, in;
		U4 lret;

		ret = -1;
		if (MapCheck(mapNode, s))
		{
			tmp = mapNode[s];
		}
		else
		{
			if (GetAddress(s, tmp))
			{
				AppLog("Unknown node ", s, " specified in ", Name);
				return;
			}
		}
		
		if (value)
		{
			StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00\x0D\x00\x08\x00\x01\x14\x01\x00", tmp, "\x01\x00\x10");
		}
		else
		{
			StrAssign(out, "\x50\x00\x00\xFF\xFF\x03\x00\x0D\x00\x08\x00\x01\x14\x01\x00", tmp, "\x01\x00\x00");
		}
			
		SendAndWait(out, in, lret);

		if (lret == 0 && StrLen(in) == 11 && StrCmp(in, "\xD0\x00\x00\xFF\xFF\x03\x00\x02\x00\x00\x00") == 0)
		{
			ret = 0;
		}
		
	EndScript

	Script Reconnect
		CVLock(cvHard);
		Command = 2;
		SetUserEvent@QSock(Sock);
		CVUnlock(cvHard);
	EndScript

	Script SendAndWait(out, in, ret)
		CVLock(cvHard);
		if (SockState == 0)
		{
			CVUnlock(cvHard);
			ret = -1;
			return;
		}
		CVLock(cv);
		Out = out;

		if (StrLen(Out))					//do not allow empty string
		{
			Command = 1;
			SetUserEvent@QSock(Sock);
			
	WaitAgain:
			Timer = CVWaitEx(cv, 2000);
			if (Timer)		//Wait function returns before time out, could be response is received or spurious wake up
			{
				if (Command)			//MessageLoop will set Command to 0 if processed, otherwise, this could be a spurious wake up
				{
					goto WaitAgain;
				}
				
				ret = Ret;
				if (ret == 0)
				{
					in = In;
				}
			}
			else			//Timer has expired
			{
				ret = -2;
			}
		}
		else
		{
			ret = -1;
		}
		CVUnlock(cv);
		CVUnlock(cvHard);

		if (ret)
		{
			AppLog("SendAndWait failure in ", Name);
			Inc(ErrorCount);
		}
	EndScript

Script MessageLoop
	U4 ret, timeout = 0;
	U0 sent_len, recv_len;
	U0 i;
	String s;

	AppLog("Message loop started for ", Name, ", script ID is ", Sys_ScriptID());

	Open:
	OpenSocket@QSock(Sock, 2, 1, 0, ret);
	if (ret)
	{
		AppLog("Failed to open socket");
		Sleep(1000);
		goto Open;
	}

	Connect:
	BindIPv4@QSock(Sock, LocalIP, LocalPort, 1, ret);
	if (ret)
	{
		AppLog("Bind returned ", ret);
		Sleep(1000);
		goto Connect;
	}

	ConnectIPv4@QSock(Sock, IP, Port, ret);
	switch (ret)
	{
	case 10035
		GetSync@QSock(Sock, i);
		LibRegSync("QSock", i);
		
	AABB:
		WaitSockEvent@QSock(Sock, event, 30000, ret);
		if (ret)
		{
			AppLog("WaitSockEvent failed with ", ret, " at case 10035.  Closing socket for ", Name);
			CloseSocket@QSock(Sock, ret);
			Sleep(1000);
			goto Open;
		}
		
		if (event & 1)
		{
			AppLog("Time out waiting for connect.  Closing socket for ", Name);
			CloseSocket@QSock(Sock, ret);
			goto Open;
		}
		
		if (event & 64)
		{
			AppLogEx(1, "Socket is connected for ", Name);
			break;
		}
		
		goto AABB;
		
	case 0
		break;
		
	default
		PrintLn("Connect returns ", ret);
		AppLog("Connect returns ", ret);
	}

	SockState = 1;
	
AAA:
	WaitSockEvent@QSock(Sock, event, 100, ret);
	if (ret)
	{
		AppLogEx(1, "WaitSockEvent returned ", ret, ".  Closing socket for ", Name);
		CloseSocket@QSock(Sock, ret);
		Sleep(1000);
		goto Open;
	}
	
BBB:
	if (event & 1)				//timeout
	{
		//perform the scan read
	}
	
	if (event & 2)				//user event
	{
		ResetUserEvent@QSock(Sock);
		switch (Command)
		{
		case 0
		AppLog("Strange, user event received without Command");
			PrintLn("Strange, user event received without Command");
			break;
			
		case 1
			Send@QSock(Sock, Out, sent_len, ret);
			if (ret)
			{
				AppLog("Failed to send message");
				PrintLn("Failed to send message");
				CloseSocket@QSock(Sock, ret);
				goto Open;
			}
			if (sent_len == StrLen(Out))
			{
				timeout = GetTick();
			}
			break;
			
			case 2
				Command = 0;
				CloseSocket@QSock(Sock, ret);
				goto Open;
		}
	}
	
	if (event & 4)				//read event
	{
		Receive@QSock(Sock, In, 65536U0, ret);
		if (ret)
		{
			AppLog("Failed to receive message at Receive");
			CloseSocket@QSock(Sock, ret);
			goto Open;
		}
		CVLock(cv);
		if (Command)
		{
			Command = 0;
			Ret = 0;
		}
		else
		{
			AppLogEx(1, "Received upexpected input from PLC");
			ret = -1;
			CVWakeAll(cv);
			CVUnlock(cv);
			CloseSocket@QSock(Sock, ret);
			goto Open;
		}
		CVWakeAll(cv);
		CVUnlock(cv);		
	}
	
	if (event & 8)				//write event
	{
	
	}
	
	if (event & 16)				//remote shutdown
	{
		AppLog("Remote site hang up");
		CloseSocket@QSock(Sock, ret);
		goto Open;
	}
	
	goto AAA;

EndScript

EndScript

Script TReadString
(tool, s, count)
String value;
U4 ret;
:tool.ReadWordConsecutive_3E(s, value, count, ret);
PrintLn("ret ", ret);
PrintLn("value in hex", StrHex(value));
PrintLn("value is ", value);
EndScript

Script TWriteString
(tool, s, value)
U4 ret;
:tool.WriteWordConsecutive_3E(s, value, ret);
PrintLn("ret ", ret);
EndScript

Script TReadW
(tool, s)
U2 value;
U4 ret;
:tool.ReadWord_3E(s, value, ret);
PrintLn("ret ", ret);
PrintLn("value ", value);
EndScript


Script TWriteW
(tool, s, x)
U4 ret;
:tool.WriteWord_3E(s, x, ret);
PrintLn("ret ", ret);
EndScript

Script TReadMulti
(tool)

U4 loop = 1;
Vector v;
PU2 pu2;
U4 ret, n = GetCount(Parameter);
Redim(pu2, n - 1);
while (loop < n)
{
	VectorPush(v, Parameter[loop]);
	Inc(loop);
}

:tool.ReadWordMulti_3E(v, pu2, ret);
PrintLn("result ", ret);
PrintLn(pu2);

EndScript

Script TGet
(tool, s)
U1 value;
U4 ret;
:tool.ReadBit_3E(s, value, ret);
PrintLn("ret ", ret);
PrintLn("data ", value);
EndScript

Script TSet
(tool, s)
U4 ret;
:tool.WriteBit_3E(s, 1, ret);
PrintLn("ret ", ret);
EndScript

Script TReset
(tool, s)
U4 ret;
:tool.WriteBit_3E(s, 0, ret);
PrintLn("ret ", ret);
EndScript

Script TReadPlan
(tool, plan, s)

PU2 pu2;
String data;
U4 ret;

if (GetCount(Parameter) == 3)
{
	:tool.ReadPlan(plan, data, 0, ret);
	if (ret)
	{
		PrintLn("Read plan failed");
	}
	else
	{
		PrintLn(data);
	}
}
else
{
	:tool.ReadPlan(plan, pu2, 1, ret);
	if (ret)
	{
		PrintLn("Read plan failed");
	}
	else
	{
		PrintLn(pu2);
	}
}

EndScript

Script TShowPlan
(tool)
Reference p;
String key;
foreach(p, :tool.mapPlan, key)
{
	PrintLn(key, " - ", StrHex(p));
}
EndScript

Script TSetupPlan
(tool, planname)
U4 loop = 2;
Vector v;
U4 ret, n = GetCount(Parameter);

while (loop < n)
{
	VectorPush(v, Parameter[loop]);
	Inc(loop);
}

if (:tool.SetupPlan(v, planname))
{
	PrintLn("Failed to setup plan");
}
else
{
	PrintLn("Plan is setup");
}

EndScript

Script TShowNode
(tool, m)

	String s, t;
	U4 pos;
	StrHex(t, StrReverse(StrSub(:tool.mapNode[m], 0, 3)), 0 ,0);
	pos = StrFindFirstNot(t, "0");
	if (pos != -1)
	{
		StrReplace(t, 0, pos, "");
	}

	switch(StrSub(:tool.mapNode[m], 3, 1))
	{

	case "\xA8"
		StrAssign(s, "D");
		break;
		
	case "\xB4"
		StrAssign(s, "W");
		break;

	case "\xA0"
		StrAssign(s, "B");
		break;

	case "\x9C"
		StrAssign(s, "X");
		break;

	case "\x9D"
		StrAssign(s, "Y");
		break;

	case "\xB0"
		StrAssign(s, "ZR");
		break;

	case "\xC4"
		StrAssign(s, "CS");
		break;

	case "\xC3"
		StrAssign(s, "CC");
		break;

	case "\xC5"
		StrAssign(s, "CN");
		break;

	case "\xA2"
		StrAssign(s, "DX");
		break;

	case "\xA3"
		StrAssign(s, "DY");
		break;

	case "\x93"
		StrAssign(s, "F");
		break;

	case "\x92"
		StrAssign(s, "L");
		break;

	case "\x90"
		StrAssign(s, "M");
		break;

	case "\xAF"
		StrAssign(s, "R");
		break;

	case "\xA1"
		StrAssign(s, "SB");
		break;

	case "\xA9"
		StrAssign(s, "SD");
		break;

	case "\x91"
		StrAssign(s, "SM");
		break;

	case "\xB5"
		StrAssign(s, "SW");
		break;

	case "\xC7"
		StrAssign(s, "STS");
		break;

	case "\xC6"
		StrAssign(s, "STC");
		break;

	case "\xC8"
		StrAssign(s, "STN");
		break;

	case "\xC1"
		StrAssign(s, "TS");
		break;

	case "\xC0"
		StrAssign(s, "TC");
		break;

	case "\xC2"
		StrAssign(s, "TN");
		break;

	case "\x94"
		StrAssign(s, "V");
		break;

	case "\xCC"
		StrAssign(s, "Z");
		break;
		
	default
		StrAssign(s, "Unknown_");
		break;
	}
	PrintLn(s, t);

EndScript

Script TShowError
(tool)
PrintLn(:tool.ErrorCount);
EndScript

Script TPHS
(tool, nodeReq, nodeReply, t1, t2, flag)
switch (GetCount(Parameter))
{
case 3
	PrintLn(:tool.PrimaryHandShake(nodeReq, nodeReply));
	break;
	
case 4
	PrintLn(:tool.PrimaryHandShake(nodeReq, nodeReply, t1));
	break;
	
case 5
	PrintLn(:tool.PrimaryHandShake(nodeReq, nodeReply, t1, t2));
	break;
	
case 6
	PrintLn(:tool.PrimaryHandShake(nodeReq, nodeReply, t1, t2, flag));
	break;
}
EndScript


Script Version
return "Mitsubishi 1.0.1";
//CW 1.0.1 7 May 2022 - Added Reconnect
EndScript
