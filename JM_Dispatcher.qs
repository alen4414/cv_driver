//To receive JM from EQ Master
Script JM_Handler
(name, type, ticket, msg, ip, port, sb)

Boolean T = 1,F = 0;
U2 temp;
Object o, o2,ack;
String s,node,writeSTR="";
U4 ret;
String tool,pp="P";

U4 loop = 0;

switch (type)
{
case 0		//Message is sent
	//PrintLn(name, ": message sent, ticket = ", ticket);
	// :Connect = 1;
	break;
	
case 1		//message send failed
	PrintLn(name, ": message send failed, ticket = ", ticket);
	// :Connect = 0;
	break;
	
case 2		
	//message received
	PrintLn(name, ": message received from ", ip, " ", port);
	PrintLn(msg);
	//Assuming this is a JSON message
	JSONParse(o, msg, ret);
	if (ret)
	{
        PrintLn("Failed to parse JSON ");
		EventLogEx(1,"Failed to parse JSON ",ret);
		return;
	}
    if(:TxID >= 65535)
    {
        :TxID = 0;
    }
    //收到JM回覆TxName_R
    GetJSON("SAMPLE_R",ack);
    //StrAppend(ack["TxName"],o["TxName"],"_R");
    StrAssign(ack["TxName"],o["TxName"],"_R");
    ack["TxID"] = o["TxID"];
    ack["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
    Send@JM(:JM, ToJSON(ack), :KERNEL.IP, :KERNEL.Port, ticket, ret);

    switch(o["TxName"])
    {
        case "QUERYIOPORTSTATUS"
            U2 IOPortStatus;
            String tempS,unload,load,output,input,down,run;
 
            String a = "_IO_Port_Status";
            String portstatus = "";
            StrAssign(portstatus,pp,o["Content"]["PORTNO"],a);

            IOPortStatus = :EFEM.ContinuousGetBitinWord(portstatus,0,16);

            tempS = DecToStrBin(IOPortStatus); 
            unload = StrSub(tempS,2,1);
            load = StrSub(tempS,3,1);
            output = StrSub(tempS,11,1);
            input = StrSub(tempS,12,1);
            down = StrSub(tempS,14,1);
            run = StrSub(tempS,15,1);
            GetJSON("IOPORTSTATUS_E",o2);
            o2["TxName"] = "QUERYIOPORTSTATUS_E";
            o2["TxID"] = Inc(:TxID);
            o2["ID"] = "CV_Driver";
            o2["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
            
            if(run == "1")
            {
                o2["Content"]["STATUS"] = "INSERVICE";
            }
            else
            {
                o2["Content"]["STATUS"] = "OUTSERVICE";
            }
            if(input == "1")
            {
                o2["Content"]["DIRECTION"] = "INMODE";
            }
            else
            {
                o2["Content"]["DIRECTION"] = "OUTMODE";
            }
            if(load =="1")
            {
                o2["Content"]["REQUEST"] = "LOADREQUEST";
            }
            if(unload =="1")
            {
                o2["Content"]["REQUEST"] = "UNLOADREQUEST";
            }
            if(load =="0" && unload =="0")
            {
                o2["Content"]["REQUEST"] = "NOREQUEST";
            }
            
            Send@JM(:JM, ToJSON(o2), :KERNEL.IP, :KERNEL.Port, ticket, ret);

        break;

        case "QUERYIOPORTPRESENCE"

            String ID1,ID2,ID3,ID4,ID5;
            String tempID1,tempID2,tempID3,tempID4,tempID5;

            GetJSON("IOPORTPRESENCECHANGE_E",o2);
            o2["TxName"] = "QUERYIOPORTPRESENCE_E";
            o2["TxID"] = Inc(:TxID);
            o2["ID"] = "CV_Driver";
            o2["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
            o2["Content"]["PORTNO"]= o["Content"]["PORTNO"];


            if(o["Content"]["STAGE"] == 1)
            {
                String g = "_Pos1_carrierID";
                String gg = "";
                StrAssign(gg,pp,o["Content"]["PORTNO"],g);

                :EFEM.ReadWordConsecutive_3E(gg, ID1, 10, ret);
                :gg = ID1;
                tempID1 = ID1;

                StrReplace(tempID1, "\x20", "");
                StrReplace(tempID1, "\x00", "");

                o2["Content"]["STAGE"] = 1;
                o2["Content"]["PRESENCE"] = "ON";
                o2["Content"]["CARRIERID"] = tempID1;
            }

            if(o["Content"]["STAGE"] == 2)
            {
                String h = "_Pos2_carrierID";
                String hh = "";
                StrAssign(hh,pp,o["Content"]["PORTNO"],h);

                :EFEM.ReadWordConsecutive_3E(hh, ID2, 10, ret);
                :hh = ID2;
                tempID2 = ID2;

                StrReplace(tempID2, "\x20", "");
                StrReplace(tempID2, "\x00", "");

                o2["Content"]["STAGE"] = 2;
                o2["Content"]["PRESENCE"] = "ON";
                o2["Content"]["CARRIERID"] = tempID2;
            }

            if(o["Content"]["STAGE"] == 3)
            {
                String j = "_Pos3_carrierID";
                String jj = "";
                StrAssign(jj,pp,o["Content"]["PORTNO"],j);

                :EFEM.ReadWordConsecutive_3E(jj, ID3, 10, ret);
                :carrierID3 = ID3;
                tempID3 = ID3;

                StrReplace(tempID3, "\x20", "");
                StrReplace(tempID3, "\x00", "");

                o2["Content"]["STAGE"] = 3;
                o2["Content"]["PRESENCE"] = "ON";
                o2["Content"]["CARRIERID"] = tempID3;
            }

            if(o["Content"]["STAGE"] == 4)
            {
                String k = "_Pos4_carrierID";
                String kk = "";
                StrAssign(kk,pp,o["Content"]["PORTNO"],k);

                :EFEM.ReadWordConsecutive_3E(kk, ID4, 10, ret);
                :carrierID4 = ID4;
                tempID4 = ID4;

                StrReplace(tempID4, "\x20", "");
                StrReplace(tempID4, "\x00", "");

                o2["Content"]["STAGE"] = 4;
                o2["Content"]["PRESENCE"] = "ON";
                o2["Content"]["CARRIERID"] = tempID4;
            }

            if(o["Content"]["STAGE"] == 5)
            {
                String m = "_Pos5_carrierID";
                String mm = "";
                StrAssign(mm,pp,o["Content"]["PORTNO"],m);

                :EFEM.ReadWordConsecutive_3E(mm, ID5, 10, ret);
                :carrierID5 = ID5;
                tempID5 = ID5;

                StrReplace(tempID5, "\x20", "");
                StrReplace(tempID5, "\x00", "");

                o2["Content"]["STAGE"] = 5;
                o2["Content"]["PRESENCE"] = "ON";
                o2["Content"]["CARRIERID"] = tempID5;
            }

            Send@JM(:JM, ToJSON(o2), :KERNEL.IP, :KERNEL.Port, ticket, ret);

        break;
        
        case "STOPIOPORTBUZZER"

            String b = "_W_IO_Port_Status";
            String buzzer = "";
            StrAssign(buzzer,pp,o["Content"]["PORTNO"],b);

            lockBit(buzzer,2,1);
            PrintLn("STOPIOPORTBUZZER change!");
            Sleep(1000);
            lockBit(buzzer,2,0);

        break;

        case "RESETIOPORTALARM"

            String c = "_W_IO_Port_Status";
            String alarm = "";
            StrAssign(alarm,pp,o["Content"]["PORTNO"],c);

            lockBit(alarm,1,1);
            PrintLn("RESETIOPORTALARM change!");
            Sleep(1000);
            lockBit(alarm,1,0);

        break;
            
        //lockContinuousBit("W1000",0,"10100000");
        
        case "E84ACTIVESET"

            String d = "_W_E84";
            String E84 = "";
            StrAssign(E84,pp,o["Content"]["PORTNO"],d);    

            StrAppend(writeSTR,o["Content"]["CS_0"],o["Content"]["CS_1"],o["Content"]["VALID"],o["Content"]["TR_REQ"],o["Content"]["BUSY"],o["Content"]["COMPT"],o["Content"]["CONT"],o["Content"]["AM_AVBL"]);
            lockContinuousBit(E84,0,writeSTR);

            PrintLn("E84ACTIVESET change!");
        break;


        case "SETUPIOPORTDIRECTION"

            String e = "_W_IO_Port_Status";
            String DIRECTION = "";
            StrAssign(DIRECTION,pp,o["Content"]["PORTNO"],e);

            if(o["Content"]["DIRECTION"] == "INMODE")
            {
                lockContinuousBit(DIRECTION,14,"10");
                PrintLn("IO_Port_Status DIRECTION change!");
            }

            if(o["Content"]["DIRECTION"] == "OUTMODE")
            {
                lockContinuousBit(DIRECTION,14,"01");
                PrintLn("IO_Port_Status DIRECTION change!");
            }
            Sleep(1000);
            lockContinuousBit(DIRECTION,14,"00");
        break;

        case "IOPORTIDREAD"

            String f = "_W_IO_Port_Status";
            String IDREAD = "";
            StrAssign(IDREAD,pp,o["Content"]["PORTNO"],f);

            lockBit(IDREAD,5,1);
            PrintLn("IOPORTIDREAD change!");
            Sleep(1000);
            lockBit(IDREAD,5,0);    

        break;

        case "SETUPIOPORTIDREADER"

            String ggg = "_W_IO_Port_Status3";
            String IDREADer = "";
            StrAssign(IDREADer,pp,o["Content"]["PORTNO"],ggg);

            if(o["Content"]["ENABLE"] == 1)
            {
                lockBit(IDREADer,0,1);
                PrintLn("IOPORTIDREAD change!");
            }
            if(o["Content"]["ENABLE"] == 0)
            {
                lockBit(IDREADer,0,0);
                PrintLn("IOPORTIDREAD change!");
            }
        break;

        case "QUERYIOPORTDIRECTION"

            String jjj = "_G_input";
            String jjjj = "";
            StrAssign(jjjj,pp,o["Content"]["PORTNO"],jjj);

            String kkk = "_G_output";
            String kkkk = "";
            StrAssign(kkkk,pp,o["Content"]["PORTNO"],kkk);


            GetJSON("QUERYIOPORTDIRECTION_E",o2);
            o2["TxID"] = Inc(:TxID);
            o2["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");

            if(:jjjj == 1)
            {
                o2["Content"]["DIRECTION"] = "OUTMODE";
            }
            if(:kkkk == 1)
            {
                o2["Content"]["DIRECTION"] = "INMODE";
            }

            Send@JM(:JM, ToJSON(o2), :KERNEL.IP, :KERNEL.Port, ticket, ret);
        break;
    }
	break;
	
case 3		//request is received
	PrintLn(name, " received request from ", ip, " ", port, ", sb = ", sb);
	SendReply(name, "I am here", ip, port, sb, ticket);
	PrintLn("SendReply ticket is ", ticket);
	break;
	
case 4		//Reply is received
	PrintLn(name, " received reply, ticket = ", ticket);
	//PrintLn(msg);
	break;
	
case 5
	PrintLn(name, " failed to receive reply");
	break;
	
case 6 
	PrintLn(name, " failed to receive message");			//unlikely to happen unless network condition is poor
	break;
	
default
	PrintLn("Unhandled type ", type);
	
}
EndScript


