Script KERNELC
	(ip,port)
    String IP = ip;
    U4 Port = port;
EndScript

Script init_getval
    (number)
    String tempS,s,key;
    U4 ret;
    String pp = "P";

    String a = "_IO_Port_Status";
    String portstatus = "";
    StrAssign(portstatus,pp,number,a);
    GCreateEx(portstatus,"U2");

    a = "_BCR_data";
    String BCR = "";
    StrAssign(BCR,pp,number,a);
    GCreateEx(BCR,"String");

    a = "_R_E84";
    String E84 = "";
    StrAssign(E84,pp,number,a);
    GCreateEx(E84,"String");

    a = "_IO_Port_Status2";
    String carrierpos = "";
    StrAssign(carrierpos,pp,number,a);
    GCreateEx(carrierpos,"String");

    // String b = "_Pos1_carrierID";
    // String CarrierID1 = "";
    // StrAssign(CarrierID1,pp,number,b);
    // GCreateEx(CarrierID1,"U2");
//
    // a = "_input";
    // String ginput = "";
    // StrAssign(ginput,pp,number,a);
    // GCreateEx(ginput,"U2");


    // GCreateEx("G_input","U2");
    // GCreateEx("G_output","U2");


    // GCreateEx("carrierID1","String");
    // GCreateEx("carrierID2","String");
    // GCreateEx("carrierID3","String");
    // GCreateEx("carrierID4","String");
    // GCreateEx("carrierID5","String");

    :EFEM.ReadWordConsecutive_3E(BCR, :BCR, 10, ret);
    :portstatus = :EFEM.ContinuousGetBitinWord(portstatus,0,16);
    :E84 = :EFEM.ContinuousGetBitinWord(E84,0,16);
    :carrierpos = :EFEM.ContinuousGetBitinWord(carrierpos,0,5);



    // :EFEM.ReadWordConsecutive_3E("P1_Pos1_carrierID", :carrierID1, 10, ret);
    // :EFEM.ReadWordConsecutive_3E("P1_Pos2_carrierID", :carrierID2, 10, ret);
    // :EFEM.ReadWordConsecutive_3E("P1_Pos3_carrierID", :carrierID3, 10, ret);
    // :EFEM.ReadWordConsecutive_3E("P1_Pos4_carrierID", :carrierID4, 10, ret);
    // :EFEM.ReadWordConsecutive_3E("P1_Pos5_carrierID", :carrierID5, 10, ret);
    

    tempS = DecToStrBin(:portstatus);


    a = "_G_input";
    String input = "";
    StrAssign(input,pp,number,a);
    GCreateEx(input,"String");

    a = "_G_output";
    String output = "";
    StrAssign(output,pp,number,a);
    GCreateEx(output,"String");

    :input = StrSub(tempS,12,1);
    :output = StrSub(tempS,11,1);
    


EndScript
    
Script checkE84
    (number)
    U2 L_E84;
    Object o,o2;
    String tempS,vs1,vs0,va,es,ho_avbl,u_req,l_req,ready;
    U4 ticket ,ret;

    String a = "_R_E84";
    String aa = "P";
    String aaa = "";
    StrAssign(aaa,aa,number,a);
    
    while(1)
    {
        L_E84 = :EFEM.ContinuousGetBitinWord(aaa,0,16);

        if(:TxID >= 65535)
        {
            :TxID = 0;
        }

        if(:aaa!=L_E84)
        {
            PrintLn("E84 Read change!");
            tempS = DecToStrBin(L_E84); 
            vs1 = StrSub(tempS,15,1);
            vs0 = StrSub(tempS,14,1);
            va = StrSub(tempS,13,1);
            es = StrSub(tempS,12,1);
            ho_avbl = StrSub(tempS,11,1);
            u_req = StrSub(tempS,10,1);
            l_req = StrSub(tempS,9,1);
            ready = StrSub(tempS,8,1);

            GetJSON("E84PASSIVE_E",o);
            o["TxID"] = Inc(:TxID);
            o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
            o["Content"]["PORTNO"] = number;
            o["Content"]["VS_1"] = vs1;
            o["Content"]["VS_0"] = vs0;
            o["Content"]["VA"] = va;
            o["Content"]["ES"] = es;
            o["Content"]["HO_AVBL"] = ho_avbl;
            o["Content"]["U_REQ"] = u_req;
            o["Content"]["L_REQ"] = l_req;
            o["Content"]["READY"] = ready;

            Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);

            :aaa = L_E84;

        }
        Sleep(1000);
    }

EndScript

Script checkIOPortStatus
    (number)
    U2 IOPortStatus;
    Object o,o2;
    String tempS,unload,load,output,input,down,run;
    U4 ticket ,ret;

    String a = "_IO_Port_Status";
    String aa = "P";
    String aaa = "";
    StrAssign(aaa,aa,number,a);

    // StrAssign(abc,aa,number,a);

    while(1)
    {
        IOPortStatus = :EFEM.ContinuousGetBitinWord(aaa,0,16);

        if(:TxID >= 65535)
        {
            :TxID = 0;
        }

        if(IOPortStatus != :aaa)
        {
            PrintLn("IOPortStatus change!");
            tempS = DecToStrBin(IOPortStatus); 
            unload = StrSub(tempS,2,1);
            load = StrSub(tempS,3,1);
            output = StrSub(tempS,11,1);
            input = StrSub(tempS,12,1);
            down = StrSub(tempS,14,1);
            run = StrSub(tempS,15,1);

            GetJSON("IOPORTSTATUS_E",o);
            
            o["TxID"] = Inc(:TxID);
            o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
            o["Content"]["PORTNO"] = number;

            if(run == "1")
            {
                o["Content"]["STATUS"] = "INSERVICE";
            }
            if(down == "1")
            {
                o["Content"]["STATUS"] = "OUTSERVICE";
            }
            if(input == "1")
            {
                o["Content"]["DIRECTION"] = "INMODE";
            }
            if(output == "1")
            {
                o["Content"]["DIRECTION"] = "OUTMODE";
            }
            if(load =="1")
            {
                o["Content"]["REQUEST"] = "LOADREQUEST";
            }
            if(unload =="1")
            {
                o["Content"]["REQUEST"] = "UNLOADREQUEST";
            }
            if(load =="0" && unload =="0")
            {
                o["Content"]["REQUEST"] = "NOREQUEST";
            }
            
            Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
            
            String jjj = "_G_input";
            String jjjj = "";
            StrAssign(jjjj,aa,o["Content"]["PORTNO"],jjj);

            String kkk = "_G_output";
            String kkkk = "";
            StrAssign(kkkk,aa,o["Content"]["PORTNO"],kkk);


            if(output != :jjjj || input != :kkkk)
            {
                GetJSON("QUERYIOPORTDIRECTION_E",o2);
                o2["TxName"] = "IOPORTDIRECTION_E";
                o2["TxID"] = Inc(:TxID);
                o2["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
                o2["Content"]["DIRECTION"] = o["Content"]["DIRECTION"];

                Send@JM(:JM, ToJSON(o2), :KERNEL.IP, :KERNEL.Port, ticket, ret);
                :jjjj = output;
                :kkkk = input;
            }

            :aaa = IOPortStatus;

        }

        Sleep(1000);
    }
EndScript

Script checkCarrierID
    (number)

    Object o;
    String ID1,ID2,ID3,ID4,ID5;
    String tempID1,tempID2,tempID3,tempID4,tempID5;

    U4 ticket ,ret,pos;

    String a = "_IO_Port_Status2";
    String aa = "P";
    String aaa = "";
    StrAssign(aaa,aa,number,a);


    while(1) //檢查每一個點位
    {
        pos = :EFEM.ContinuousGetBitinWord(aaa,0,5);

        // :EFEM.ReadWordConsecutive_3E("P1_Pos1_carrierID", ID1, 10, ret);
        // :EFEM.ReadWordConsecutive_3E("P1_Pos2_carrierID", ID2, 10, ret);
        // :EFEM.ReadWordConsecutive_3E("P1_Pos3_carrierID", ID3, 10, ret);
        // :EFEM.ReadWordConsecutive_3E("P1_Pos4_carrierID", ID4, 10, ret);
        // :EFEM.ReadWordConsecutive_3E("P1_Pos5_carrierID", ID5, 10, ret);

        if(:TxID >= 65535)
        {
            :TxID = 0;
        }

        if(:aaa != pos)
        {
            switch (pos)
            {
                case 1
                    String b = "_Pos1_carrierID";
                    String bb = "";
                    StrAssign(bb,aa,number,b);
                    PrintLn("CarrierID 1 change!");

                    :EFEM.ReadWordConsecutive_3E(bb, ID1, 10, ret);
                    GetJSON("IOPORTPRESENCECHANGE_E",o);
                    tempID1 = ID1;
                    StrReplace(tempID1, "\x20", "");
                    StrReplace(tempID1, "\x00", "");
                    o["TxID"] = Inc(:TxID);
                    o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
                    o["Content"]["STAGE"] = 1;
                    o["Content"]["PRESENCE"] = "ON";
                    o["Content"]["CARRIERID"] = tempID1;
                    
                    Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
                    break;

                case 2
                    String c = "_Pos2_carrierID";
                    String cc = "";
                    StrAssign(cc,aa,number,c);
                    PrintLn("CarrierID 2 change!");

                    :EFEM.ReadWordConsecutive_3E(cc, ID2, 10, ret);
                    GetJSON("IOPORTPRESENCECHANGE_E",o);
                    tempID2 = ID2;
                    StrReplace(tempID2, "\x20", "");
                    StrReplace(tempID2, "\x00", "");
                    o["TxID"] = Inc(:TxID);
                    o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
                    o["Content"]["STAGE"] = 2;
                    o["Content"]["PRESENCE"] = "ON";
                    o["Content"]["CARRIERID"] = tempID2;
                    
                    Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
                    break;
                
                case 4
                    String d = "_Pos3_carrierID";
                    String dd = "";
                    StrAssign(dd,aa,number,d);
                    PrintLn("CarrierID 3 change!");

                    :EFEM.ReadWordConsecutive_3E(dd, ID3, 10, ret);
                    GetJSON("IOPORTPRESENCECHANGE_E",o);
                    tempID3 = ID3;
                    StrReplace(tempID3, "\x20", "");
                    StrReplace(tempID3, "\x00", "");
                    o["TxID"] = Inc(:TxID);
                    o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
                    o["Content"]["STAGE"] = 3;
                    o["Content"]["PRESENCE"] = "ON";
                    o["Content"]["CARRIERID"] = tempID3;
                    
                    Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
                    break;
                
                case 8
                    String e = "_Pos4_carrierID";
                    String ee = "";
                    StrAssign(ee,aa,number,e);
                    PrintLn("CarrierID 4 change!");

                    :EFEM.ReadWordConsecutive_3E(ee, ID4, 10, ret);
                    GetJSON("IOPORTPRESENCECHANGE_E",o);
                    tempID4 = ID4;
                    StrReplace(tempID4, "\x20", "");
                    StrReplace(tempID4, "\x00", "");
                    o["TxID"] = Inc(:TxID);
                    o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
                    o["Content"]["STAGE"] = 4;
                    o["Content"]["PRESENCE"] = "ON";
                    o["Content"]["CARRIERID"] = tempID4;
                    
                    Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
                    break;

                case 16
                    String f = "_Pos5_carrierID";
                    String ff = "";
                    StrAssign(ff,aa,number,f);
                    PrintLn("CarrierID 5 change!");

                    :EFEM.ReadWordConsecutive_3E(ff, ID5, 10, ret);
                    GetJSON("IOPORTPRESENCECHANGE_E",o);
                    tempID5 = ID5;
                    StrReplace(tempID5, "\x20", "");
                    StrReplace(tempID5, "\x00", "");
                    o["TxID"] = Inc(:TxID);
                    o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
                    o["Content"]["STAGE"] = 5;
                    o["Content"]["PRESENCE"] = "ON";
                    o["Content"]["CARRIERID"] = tempID4;
                    
                    Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
                    break;
                

            }
            :aaa = pos;
        }

        // if(ID1 !=  :carrierID1)
        // {   
        //     PrintLn("CarrierID 1 change!");

        //     tempID1 = ID1;
        //     StrReplace(tempID1, "\x20", "");
        //     StrReplace(tempID1, "\x00", "");
        //     GetJSON("IOPORTPRESENCECHANGE_E",o);
            
        //     o["TxID"] = Inc(:TxID);
        //     o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
        //     o["Content"]["STAGE"] = 1;
        //     o["Content"]["PRESENCE"] = "ON";
        //     o["Content"]["CARRIERID"] = tempID1;
            
        //     Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
        //     :carrierID1 = ID1;
        // }

        // if(ID2 !=  :carrierID2)
        // {   
        //     PrintLn("CarrierID 2 change!");

        //     tempID2 = ID2;
        //     StrReplace(tempID2, "\x20", "");
        //     StrReplace(tempID2, "\x00", "");
        //     GetJSON("IOPORTPRESENCECHANGE_E",o);
            
        //     o["TxID"] = Inc(:TxID);
        //     o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
        //     o["Content"]["STAGE"] = 2;
        //     o["Content"]["PRESENCE"] = "ON";
        //     o["Content"]["CARRIERID"] = tempID2;
            
        //     Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
        //     :carrierID2 = ID2;
        // }

        // if(ID3 !=  :carrierID3)
        // {   
        //     PrintLn("CarrierID 3 change!");

        //     tempID3 = ID3;
        //     StrReplace(tempID3, "\x20", "");
        //     StrReplace(tempID3, "\x00", "");
        //     GetJSON("IOPORTPRESENCECHANGE_E",o);
            
        //     o["TxID"] = Inc(:TxID);
        //     o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
        //     o["Content"]["STAGE"] = 3;
        //     o["Content"]["PRESENCE"] = "ON";
        //     o["Content"]["CARRIERID"] = tempID3;
            
        //     Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
        //     :carrierID3 = ID3;
        // }

        // if(ID4 !=  :carrierID4)
        // {   
        //     PrintLn("CarrierID 4 change!");

        //     tempID4 = ID4;
        //     StrReplace(tempID4, "\x00", "");
        //     StrReplace(tempID4, "\x20", "");
        //     GetJSON("IOPORTPRESENCECHANGE_E",o);
            
        //     o["TxID"] = Inc(:TxID);
        //     o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
        //     o["Content"]["STAGE"] = 4;
        //     o["Content"]["PRESENCE"] = "ON";
        //     o["Content"]["CARRIERID"] = tempID4;
            
        //     Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
        //     :carrierID4 = ID4;
        // }

        // if(ID5 !=  :carrierID5)
        // {   
        //     PrintLn("CarrierID 5 change!");
        //     tempID5 = ID5;
        //     StrReplace(tempID5, "\x00", "");
        //     StrReplace(tempID5, "\x20", "");
        //     GetJSON("IOPORTPRESENCECHANGE_E",o);
            
        //     o["TxID"] = Inc(:TxID);
        //     o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
        //     o["Content"]["STAGE"] = 5;
        //     o["Content"]["PRESENCE"] = "ON";
        //     o["Content"]["CARRIERID"] = tempID5;
            
        //     Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
        //     :carrierID5 = ID5;
        // }

        Sleep(1000);
    }

EndScript

Script checkBCR
    (number)
    String BCR,temp;
    Object o;
    U4 ticket ,ret;

    String a = "_BCR_data";
    String aa = "P";
    String aaa = "";
    StrAssign(aaa,aa,number,a);



    while(1) //檢查每一個點位
    {
        :EFEM.ReadWordConsecutive_3E(aaa, BCR, 10, ret);

        if(:TxID >= 65535)
        {
            :TxID = 0;
        }

        if(BCR !=  :aaa)
        {   
            PrintLn("BCR_data change!");

            temp = BCR;
            StrReplace(temp, "\x00", "");
            StrReplace(temp, "\x20", "");
            GetJSON("IOPORTREADER_E",o);
            
            o["TxID"] = Inc(:TxID);
            o["TimeStamp"] = StrTime("%Y%M%D%h%m%s%3");
            o["Content"]["CARRIERID"] = temp;
            
            Send@JM(:JM, ToJSON(o), :KERNEL.IP, :KERNEL.Port, ticket, ret);
            :aaa = BCR;
        }

        Sleep(1000);
    }
EndScript


Script bbb

    //lockBit("P1_R_E84",6,1);
    // lockContinuousWord("P1_Pos1_carrierID","1111",0);
    // lockContinuousWord("P1_Pos2_carrierID","2222",0);
    // lockContinuousWord("P1_Pos3_carrierID","3333",0);
    // lockContinuousWord("P1_Pos4_carrierID","4444",0);
    // lockContinuousWord("P1_Pos5_carrierID","5555",0);
    //lockContinuousWord("P1_BCR_data","bbbb",0);

EndScript

Script ccc
    U4 ret;
    String s,a,b,c;

    :EFEM.ReadWordConsecutive_3E("P1_Pos1_carrierID", s, 10, ret);  
    //ReadWordConsecutive_3E("D6407", 回傳的String, 幾位數, 是否成功);
    a = :EFEM.GetBitinWord("P1_W_IO_Port_Status",14);
    b = :EFEM.GetBitinWord("P1_W_IO_Port_Status",5);

    PrintLn("value is ",s);
    PrintLn("value is ",a);
    PrintLn("value is ",b);
    
EndScript
