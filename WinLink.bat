mkdir %1

mklink %1\DriverSystem.qp %~dp0DriverSystem.qp
mklink %1\Copyright.txt %~dp0Copyright\Copyright.txt
mklink %1\DriverSystem.qs %~dp0DriverSystem.qs
mklink %1\common.qs %~dp0Common\common.qs
mklink %1\Mitsubishi.qs %~dp0Mit\Mitsubishi.qs
mklink %1\JM_Dispatcher.qs %~dp0JM_Dispatcher.qs
mklink %1\Config.json %~dp0Config.json
mklink %1\EFEM.json %~dp0PLC_Config_EM\EFEM.json

